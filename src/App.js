import React, { useState } from "react";

import "./App.css";

/**
 * below you will find panels and buttons being rendered for each
 * 'panel' described in the intialPanels configuration. Using hooks,
 * adjust the code so that when a button is clicked, the corresponding 'panel'
 * with that id is moved to the top of the rendered panels. You can change as
 * much or as little of the code as you would like to achieve this.
 */

const initialPanels = [
  { id: 0, color: "navy" },
  { id: 1, color: "orange" },
  { id: 2, color: "purple" },
  { id: 3, color: "violet" }
];

function Panel(props) {
  return (
    <div
      style={{ backgroundColor: props.color, width: "100%", height: "100px" }}
    />
  );
}

function FixedControls(props) {
  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        width: "100px",
        height: "100px"
      }}
    >
      {props.panels.map(({id,color}) => {
        return (
          <button onClick={() => props.handleClick(id)} key={"button-" + id }>
         panel {id} {color}
          </button>
        );
      })}
    </div>
  );
}

function RenderPanels(props) {
  return props.panels.map(({ id, color }) => (
    <Panel key={"panel-" + id} color={color} />
  ));
}

function App() {
  const [panels, setPanels] = useState(initialPanels);

  const handleClick = id => {
   let newPanels = panels;
   const selectedPanel = panels.find(panel=> panel.id === id)
   newPanels = ([selectedPanel]).concat(panels.filter(panel => panel.id !== id))
   setPanels(newPanels)

  };
  
  return (
    <>
      <FixedControls handleClick={handleClick} panels={panels} />
      <RenderPanels panels={panels} />
    </>
  );
}
export default App;
